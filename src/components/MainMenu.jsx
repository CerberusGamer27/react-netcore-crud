import 'bootstrap/dist/css/bootstrap.min.css';
import axiosClient from "../config/axiosClient.jsx";
import { Modal, ModalBody, ModalFooter, ModalHeader } from 'reactstrap'
import { useEffect, useState } from "react";
//import '../App.css'

function MainMenu() {
    const [data, setData] = useState([]);
    const [insertModal, setInsertModal] = useState(false);
    const [editModal, setEditModal] = useState(false);
    const [deleteModal, setDeleteModal] = useState(false);
    const [dbms, setDbms] = useState({
        id: '',
        nombre: '',
        lanzamiento: '',
        desarrollador: '',
    })


    // This metod allow to get the value of input text based on name attribute
    const handleChange = e => {
        const { name, value } = e.target;
        setDbms({
            ...dbms,
            [name]: value
        })
        console.log(dbms)
    }

    const toggleModalAdd = () => {
        setInsertModal(!insertModal);
    }

    const toggleModalEdit = () => {
        setEditModal(!editModal);
    }

    const toggleModalDelete = () => {
        setDeleteModal(!deleteModal);
    }

    const getData = async () => {
        await axiosClient().then(response => {
            setData(response.data)
        }).catch(error => {
            console.log(error)
        })
    }

    const postData = async () => {
        delete dbms.id;
        dbms.lanzamiento = parseInt(dbms.lanzamiento);
        await axiosClient.post('https://api.c3rberus.dev/api/gestores', dbms).then(response => {
            setData(data.concat(response.data));
            toggleModalAdd();
        }).catch(error => {
            console.log(error)
        })
    }

    const putData = async () => {
        dbms.lanzamiento = parseInt(dbms.lanzamiento);
        await axiosClient.put(`https://api.c3rberus.dev/api/gestores/${dbms.id}`, dbms).then(response => {
            var respuesta = response.data;
            var auxData = data
            auxData.map(db => {
                if (db.id === dbms.id) {
                    db.nombre = respuesta.nombre;
                    db.lanzamiento = respuesta.lanzamiento;
                    db.desarrollador = respuesta.desarrollador
                }
            });
            toggleModalEdit();
        }).catch(error => {
            console.log(error)
        })
    }

    const deleteData = async () => {
        await axiosClient.delete(`https://api.c3rberus.dev/api/gestores/${dbms.id}`, dbms).
            then(response => {
                setData(data.filter(gestor => gestor.id !== response.data))
                toggleModalDelete();
            }).catch(error => {
                console.log(error)
            })
    }

    const selectDbms = (gestor, caso) => {
        setDbms(gestor);
        (caso === "Editar") ? toggleModalEdit() : toggleModalDelete();
    }

    useEffect(() => {
        getData();
    }, [])

    return (
        <div>
            <button className='btn btn-primary my-5' onClick={() => toggleModalAdd()}>Add New DBMS</button>
            <table className={"table table-bordered"}>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Name</th>
                        <th>Release</th>
                        <th>Developer</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {data.map(gestor => (
                        <tr key={gestor.id}>
                            <td>
                                {gestor.id}
                            </td>
                            <td>
                                {gestor.nombre}
                            </td>
                            <td>
                                {gestor.lanzamiento}
                            </td>
                            <td>
                                {gestor.desarrollador}
                            </td>
                            <td>
                                <button className={'btn btn-primary'} onClick={() => selectDbms(gestor, "Editar")}>Edit</button> {" "}
                                <button className={'btn btn-danger'} onClick={() => selectDbms(gestor, "Eliminar")}>Delete</button>
                            </td>
                        </tr>
                    ))
                    }
                </tbody>
            </table>

            <Modal isOpen={insertModal} toggle={toggleModalAdd}>
                <ModalHeader>Insert on DBMS</ModalHeader>
                <ModalBody>
                    <div className='form-group'>
                        <div className="mb-3">
                            <label>Name: </label>
                            <input type={"text"} name="nombre" className={"form-control"} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label>Release Date: </label>
                            <input type={"number"} name="lanzamiento" className={"form-control"} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label>Developer: </label>
                            <input type={"text"} name="desarrollador" className={"form-control"} onChange={handleChange} />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button className='btn btn-primary' onClick={() => postData()}>Add</button> {" "}
                    <button className='btn btn-danger' onClick={() => toggleModalAdd()}>Cancel</button>
                </ModalFooter>
            </Modal>

            <Modal isOpen={editModal} toggle={toggleModalEdit}>
                <ModalHeader>Edit DBMS</ModalHeader>
                <ModalBody>
                    <div className='form-group'>
                        <div className="mb-3">
                            <label>ID: </label>
                            <input type={"text"} className={"form-control"} name={"id"} onChange={handleChange} value={dbms && dbms.id} readOnly />
                        </div>
                        <div className="mb-3">
                            <label>Name: </label>
                            <input type={"text"} className={"form-control"} name={"nombre"} onChange={handleChange} value={dbms && dbms.nombre} />
                        </div>
                        <div className="mb-3">
                            <label>Release Date: </label>
                            <input type={"number"} className={"form-control"} name={"lanzamiento"} onChange={handleChange} value={dbms && dbms.lanzamiento} />
                        </div>
                        <div className="mb-3">
                            <label>Developer: </label>
                            <input type={"text"} className={"form-control"} name={"desarrollador"} onChange={handleChange} value={dbms && dbms.desarrollador} />
                        </div>
                    </div>
                </ModalBody>
                <ModalFooter>
                    <button className='btn btn-primary' onClick={() => putData()}>Add</button> {" "}
                    <button className='btn btn-danger' onClick={() => toggleModalEdit()}>Cancel</button>
                </ModalFooter>
            </Modal>

            <Modal isOpen={deleteModal} toggle={toggleModalDelete}>
                <ModalHeader>Delete {dbms && dbms.desarrollador}</ModalHeader>
                <ModalBody>
                    Are you sure that want to delete this DBMS?
                </ModalBody>
                <ModalFooter>
                    <button className='btn btn-primary' onClick={() => deleteData()}>Yes</button> {" "}
                    <button className='btn btn-danger' onClick={() => toggleModalDelete()}>No</button>
                </ModalFooter>
            </Modal>
        </div>
    )
}

export default MainMenu;