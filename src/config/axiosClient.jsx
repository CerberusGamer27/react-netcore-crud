import axios from "axios";

const axiosClient = axios.create({
    //baseURL: 'http://localhost:7118/api/gestores'
    baseURL: `https://api.c3rberus.dev/api/gestores`
});

export default axiosClient;